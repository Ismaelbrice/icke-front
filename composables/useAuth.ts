import type { Users } from "~/entities"

export const useAuth = () => {
  return useCookie<Users|null>('user', {
    sameSite: 'strict'
  });
}
