import type { UseFetchOptions } from "#app";


export function useApi<T>(
    url: string | (() => string),
    options: UseFetchOptions<T> = {}
) {
    return useFetch(url, {
        ...options,
        baseURL: 'http://localhost:8080',
        credentials: 'include',
        headers: useRequestHeaders(['cookie'])
    });
}



export const $api = $fetch.create({
    baseURL: 'http://localhost:8080',
    credentials: 'include',
    onRequest(context) {
        context.options.headers = {
            'X-Requested-With': 'XMLHttpRequest',
            ...useRequestHeaders(['cookie'])
        }
    },
    onResponseError(context) {
        if(context.response.status == 401) {
            useAuth().value = null;
            navigateTo('/login');
        }
    }
    
})
