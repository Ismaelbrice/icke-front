export interface Users {
    id?:number;
    email:string;
    name:string;
    password?:string;
    role?:string;
    adress:string;
    genre:string;
    

}

export interface Images{
    id?:number;
    picture:string;
    produits:Produits;

}

export interface LineProduits{

}

export interface Categories{
    id?:number;
    categorie:string;
    produits: Produits[];
}

export interface Produits{
    id?:number;
    title:string ;
    description:string;
    size:string;
    color:string;
    price:number;
    genre:string;
    marque:string;
    mainPicture?:string;
    lineproduits?:LineProduits;
    categories:Categories;
    images: Images[]
}

export interface Page<T> {
    content: T[];
    totalPages:number;
    totalElements:number;
    last:boolean;
    first:boolean;
}
